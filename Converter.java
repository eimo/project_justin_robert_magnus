public class Converter {
//Your names go here:
/*
* @Author: Justin Hung
* Robert  Dumitru
* Magnus Lyngberg
*
*/
private double celsiusToFahrenheit(double C){
return C*(9/5)+32;
}
private double fahrenheitToCelsius(double F){
 double cel = (F-32)*(5/9);
return cel;
}
public static void main(String[] args) {
Converter test = new Converter(); 
test.celsiusToFahrenheit(180); 
test.fahrenheitToCelsius(250); 
}
}